import React, { Component } from 'react';

class Login extends Component {
  render() {
    return(
      <a className="btn btn-primary" 
         href="http://api1.494902.xyz/oauth/google" 
         role="button">Login with Google</a>);
  }
}

export default Login;